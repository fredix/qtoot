#include "account.h"
#include "ui_account.h"

Account::Account(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Account)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    settings = new QIniSettings("qtoot", "qtoot");

    settings->beginGroup("Mastodon");
    ui->lineEdit_login->setText(settings->value(QString::fromUtf8("Login")).toString());
    ui->lineEdit_password->setText(settings->value(QString::fromUtf8("Password")).toString());
    ui->lineEdit_url->setText(settings->value(QString::fromUtf8("Url")).toString());
    settings->endGroup();
}

Account::~Account()
{
    delete ui;
}

void Account::on_buttonBox_accepted()
{
    QHash<QString, QString> hash;

    hash["login"] = ui->lineEdit_login->text();
    hash["password"] = ui->lineEdit_password->text();
    hash["url"] = ui->lineEdit_url->text();
    qDebug() << "ACCOUNT : " << hash;

    settings->beginGroup("Mastodon");
    settings->setValue(QString::fromUtf8("Login"), hash["login"]);
    settings->setValue(QString::fromUtf8("Password"), hash["password"]);
    settings->setValue(QString::fromUtf8("Url"), hash["url"]);
    settings->endGroup();

    settings->sync();
}
