#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QPointer>

#include "toot.h"
#include "about.h"
#include "account.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_tabWidget_currentChanged(int index);
    void onResult(QNetworkReply *reply);

    void on_actionabout_triggered();

    void on_actionaccount_triggered();

private:
    Ui::MainWindow *ui;
    QNetworkAccessManager* nam;
    QPointer<about> aboutDlg;
    QPointer<Account> accountDlg;

};

#endif // MAINWINDOW_H
