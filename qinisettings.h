#ifndef QINISETTINGS_H
#define QINISETTINGS_H

#include <QSettings>

class QIniSettings : public QSettings {
  Q_OBJECT
  Q_DISABLE_COPY (QIniSettings)

public:
  QIniSettings(const QString &organization = "pcode", const QString &application = "nodecast", QObject *parent = 0 ):
#ifdef Q_OS_WIN
      QSettings(QSettings::IniFormat, QSettings::UserScope, organization, application, parent)
#else
      QSettings(organization, application, parent)
#endif
  {

  }

  QIniSettings(const QString &fileName, Format format, QObject *parent = 0 ) : QSettings(fileName, format, parent) {

  }

#ifdef Q_OS_WIN
  QVariant value(const QString & key, const QVariant &defaultValue = QVariant()) const {
    QString key_tmp(key);
    QVariant ret = QSettings::value(key_tmp);
    if (ret.isNull())
      return defaultValue;
    return ret;
  }

  void setValue(const QString &key, const QVariant &val) {
    QString key_tmp(key);
    if (format() == QSettings::NativeFormat) // Using registry, don't touch replace here
      key_tmp.replace("\\", "/");
    QSettings::setValue(key_tmp, val);
  }
#endif
};

#endif // QINISETTINGS_H
