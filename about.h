#ifndef ABOUT_H
#define ABOUT_H

#include "ui_about.h"
#include <QFile>
#include <QtGlobal>

class about : public QDialog, private Ui::AboutDlg{
  Q_OBJECT

  public:
    ~about() {
      qDebug("Deleting about dlg");
    }

    about(QWidget *parent): QDialog(parent) {
      setupUi(this);
      setAttribute(Qt::WA_DeleteOnClose);
      // About
      QString aboutText =
          QString::fromUtf8("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\"><html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">p, li { white-space: pre-wrap; }</style></head><body style=\" font-size:11pt; font-weight:400; font-style:normal;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">") +
          tr("A mastodon client software programmed in Qt/C++.") +
          QString::fromUtf8("<br /><br />") +
          tr("Home Page: ") +
          QString::fromUtf8("<a href=\"https://framagit.org/fredix/qtoot/\"><span style=\" text-decoration: underline;\">https://framagit.org/fredix/qtoot/</span></a></p><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">") +
          tr("Source code: ") +
          QString::fromUtf8("<a href=\"https://framagit.org/fredix/qtoot/\"><span style=\" text-decoration: underline;\">https://framagit.org/fredix/qtoot/</span></a><br />") +
          tr("IRC: #qtoot on Freenode") +
          QString::fromUtf8(
              "</p></body></html>");
      lb_about->setText(aboutText);
      // Set icons
      logo->setPixmap(QPixmap(QString::fromUtf8(":/img/logo/qtoot_logo.png")));
      //Title
      lb_name->setText(QString::fromUtf8("<b><h1>qtoot")+QString::fromUtf8(" 0.1 </h1></b>"));
      // Thanks
      QString thanks_txt;
      thanks_txt = QString::fromUtf8("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\"><html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">p, li { white-space: pre-wrap; }</style></head><body style=\" font-size:11pt; font-weight:400; font-style:normal;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">");
      thanks_txt += QString::fromUtf8("<p>Thanks to these free software projects :</p>");
      thanks_txt += QString::fromUtf8("<ul><li><a href=\"https://joinmastodon.org\"><span style=\" text-decoration: underline;\">Mastodon project</span></a></li>");
      thanks_txt += QString::fromUtf8("<li><a href=\"https://www.qt.io\"><span style=\" text-decoration: underline;\">Qt</span></a></li></ul>");
      thanks_txt += QString::fromUtf8("and many others.</br>");
      thanks_txt += QString::fromUtf8("</p></body></html>");
      lb_thanks->setText(thanks_txt);
      // Translation
      QString trans_txt = "";
      te_translation->setHtml(trans_txt);
      // License
      te_license->append(QString::fromUtf8("<a name='top'></a>"));
      QFile licensefile(":/gpl.html");
      if (licensefile.open(QIODevice::ReadOnly|QIODevice::Text)) {
        te_license->setHtml(licensefile.readAll());
        licensefile.close();
      }
      // Libraries
      label_11->setText(QT_VERSION_STR);
      show();
    }
};


#endif // ABOUT_H
