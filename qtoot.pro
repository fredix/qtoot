#-------------------------------------------------
#
# Project created by QtCreator 2017-12-26T01:14:58
#
#-------------------------------------------------

QT       += core gui network networkauth xml multimedia
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtoot
target.path = /usr/bin
INSTALLS += target
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    account.cpp \
    toot.cpp

HEADERS += \
        mainwindow.h \
    account.h \
    toot.h \
    about.h \
    qinisettings.h

FORMS += \
        mainwindow.ui \
    about.ui \
    account.ui \
    toot.ui

#SUBDIRS += \
#    externals/qmastodon/libmastodon.pro \
#    externals/qmastodon/qmastodon.pro


INCLUDEPATH += externals/qmastodon/
LIBS += -Lexternals/qmastodon/liblibmastodon.a

DISTFILES +=
