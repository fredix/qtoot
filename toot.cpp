#include "toot.h"
#include "ui_toot.h"

Toot::Toot(QString user, QString toot, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Toot)
{
    ui->setupUi(this);
    ui->label_user->setText(user);
    ui->label_toot->setText(toot);
    //ui->label->pixmap()
}

Toot::~Toot()
{
    delete ui;
}


void Toot::getAvatar(QString avatar_url)
{
    nam = new QNetworkAccessManager(this);
    connect(nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(slot_netwManagerFinished(QNetworkReply*)));

    QUrl url(avatar_url);
    QNetworkRequest request(url);
    nam->get(request);
}



void Toot::slot_netwManagerFinished(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Error in" << reply->url() << ":" << reply->errorString();
        return;
    }

    QByteArray jpegData = reply->readAll();
    QPixmap pixmap;
    pixmap.loadFromData(jpegData);
    ui->label->setPixmap(pixmap.scaled(48, 48, Qt::IgnoreAspectRatio, Qt::FastTransformation));

}
