#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QDialog>
#include <QDebug>
#include "qinisettings.h"

namespace Ui {
class Account;
}

class Account : public QDialog
{
    Q_OBJECT

public:
    explicit Account(QWidget *parent = nullptr);
    ~Account();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Account *ui;
    QIniSettings *settings;
};

#endif // ACCOUNT_H
