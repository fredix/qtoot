#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    nam = new QNetworkAccessManager(this);
//    QObject::connect(nam, SIGNAL(finished(QNetworkReply*)),
//                   this, SLOT(onResult(QNetworkReply*)));

    connect(nam, &QNetworkAccessManager::finished, this, &MainWindow::onResult);


    QUrl url("https://mastodon.xyz/api/v1/timelines/public");
    nam->get(QNetworkRequest(url));

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::onResult(QNetworkReply *reply)
{
    reply->ignoreSslErrors();
    if(reply->error() == QNetworkReply::NoError){

        QByteArray result = reply->readAll();
        QJsonParseError err;
        QJsonDocument jsonResponse = QJsonDocument::fromJson(result, &err);

        if (err.error != QJsonParseError::NoError)
             qDebug() << "JSON parse error " << err.errorString();

        qDebug() << "json is array : " << jsonResponse.isArray();

        QJsonArray array = jsonResponse.array();

        qDebug() << "value : " << array.first().toString();

        //QJsonObject obj = jsonResponse.object();
        //qDebug()<<"id: " << obj["id"].toString();
        //QJsonArray array = obj["id"].toArray();

        for(const QJsonValue & value : array) {
           QJsonObject obj = value.toObject();
           qDebug()<< "text: " << obj["content"].toString();

           QJsonObject l_user = obj["account"].toObject();

           qDebug() << "obj keys : " << obj.keys();

           Toot *new_toot = new Toot(l_user["acct"].toString(), obj["content"].toString());
           new_toot->getAvatar(l_user["avatar"].toString());
           ui->verticalLayout_public->addWidget(new_toot);
        }
    }
    else
        qDebug() << "ERROR : " << reply->error();
    reply->deleteLater();
}




void MainWindow::on_tabWidget_currentChanged(int index)
{

}

void MainWindow::on_actionabout_triggered()
{
    //About dialog
    if (aboutDlg) {
      aboutDlg->setFocus();
    } else {
      aboutDlg = new about(this);
    }
}

void MainWindow::on_actionaccount_triggered()
{
    //Account dialog
    if (accountDlg) {
      accountDlg->setFocus();
    } else {
      accountDlg = new Account(this);
      accountDlg->show();
    }
}
