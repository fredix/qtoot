#ifndef TOOT_H
#define TOOT_H

#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkReply>

namespace Ui {
class Toot;
}

class Toot : public QWidget
{
    Q_OBJECT

public:
    explicit Toot(QString user, QString toot, QWidget *parent = nullptr);
    ~Toot();
    void getAvatar(QString url);

private:
    Ui::Toot *ui;
     QNetworkAccessManager *nam;
private slots:
     void slot_netwManagerFinished(QNetworkReply *reply);
};

#endif // TOOT_H
